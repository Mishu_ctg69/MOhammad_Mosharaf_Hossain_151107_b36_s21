<?php

namespace App\Model;
use PDO;
use PDOException;

class Database{

    public $DBH;
    public $host = "localhost";
    public $dbName = "atomic_project_b36";
    public $user = "root";
    public $password = "";

    public function __construct(){
        try
        {
        $this-> DBH = new PDO("mysql:host=$this->host; dbname=$this->dbName",                                          $this->user, $this->password);
            echo "Successfully Connected <br>";
        }
        catch(PDOException $error)
        {
                echo $error->getMessage();
        }
}
}
